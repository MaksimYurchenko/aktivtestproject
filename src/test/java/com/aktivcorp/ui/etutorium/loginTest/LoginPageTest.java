package com.aktivcorp.ui.etutorium.loginTest;

import com.aktivcorp.ui.etutorium.model.login.UserUiModel;
import com.aktivcorp.ui.etutorium.pages.homePage.HomePage;
import com.aktivcorp.ui.etutorium.pages.login.LoginPage;
import com.codeborne.selenide.Condition;
import com.codeborne.selenide.Selenide;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;

public class LoginPageTest {

    @Test
    public void testLoginByUserCred() {

        UserUiModel userUiModel = new UserUiModel();

        LoginPage
                .open()
                .loginAs(userUiModel)
                .navigatesTo(HomePage.class)
                .addWebinar.shouldHave(Condition.appear);
    }

    @AfterMethod
    public void afterMethod(){
        Selenide.clearBrowserCookies();
    }
}
