package com.aktivcorp.ui.etutorium.loginTest;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.*;
import org.openqa.selenium.*;

public class RecorderTest {

    private WebDriver driver;

    @BeforeClass(alwaysRun = true)
    public void setUp() throws Exception {
        System.setProperty("webdriver.chrome.driver", "C:\\Users\\user\\Downloads\\chromedriver_win321\\chromedriver.exe");
        driver = new ChromeDriver();
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
    }

    @Test
    public void testUntitledTestCase() throws Exception {
        driver.get("https://passport.etutorium.com/auth/login/etutorium/ru");
        driver.findElement(By.id("login_username")).sendKeys("regtstr@gmail.com");
        driver.findElement(By.id("login_password")).sendKeys("qaz111");
        driver.findElement(By.xpath("//button[@type='submit']")).click();
        driver.findElement(By.cssSelector("div.header-container__button-icon > svg > use")).click();
        driver.findElement(By.xpath("//form/div[2]")).click();
        driver.findElement(By.xpath("//div[3]/label/div")).click();
        driver.findElement(By.xpath("//div[9]/button")).click();
        driver.findElement(By.xpath("//textarea")).sendKeys("test");
        driver.findElement(By.xpath("//div[9]/button")).click();
    }

    @AfterClass(alwaysRun = true)
    public void tearDown() throws Exception {
        driver.quit();
    }
}



