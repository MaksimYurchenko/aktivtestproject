package com.aktivcorp.api.geocoder.addressTests;

import com.aktivcorp.api.geocoder.models.addressModels.getAddressModel.GetAddressModelResponse;
import com.aktivcorp.api.geocoder.models.addressModels.postAddressModel.Options;
import com.aktivcorp.api.geocoder.models.addressModels.postAddressModel.PostAddressModel;
import com.aktivcorp.api.geocoder.models.addressModels.postAddressModel.response.PostAddressModelResponse;
import com.aktivcorp.api.geocoder.service.addressService.AddressService;
import org.testng.Assert;
import org.testng.annotations.Test;

import static com.aktivcorp.api.geocoder.conditions.Conditions.bodyField;
import static com.aktivcorp.api.geocoder.conditions.Conditions.statusCode;
import static com.aktivcorp.api.geocoder.properties.ApiKeys.API_KEY;
import static com.aktivcorp.utilites.dataGenerator.UserDataGeneratorV3.getFakerAddress;
import static org.hamcrest.Matchers.containsString;
import static org.testng.Assert.assertEquals;

public class AddressTests {

    private AddressService addressService = new AddressService();

    String address ="Zaporizhzhia, Peremohy st 77";

    @Test
    public void testPostAddressSuccess(){

        Options options = new Options().randomize();

        PostAddressModel postAddressModel = new PostAddressModel().randomize(options)
                .setLocation(address);

        PostAddressModelResponse postAddressModelResponse = addressService
                .post_addressService(API_KEY, postAddressModel)
                .shouldHave(statusCode(200))
                .responseAs(PostAddressModelResponse.class);

        assertEquals(postAddressModelResponse.getInfo().getStatuscode(), 0);
        assertEquals(postAddressModelResponse.getResults().get(0).getLocations().get(0).getStreet(), "Peremohy Street");
        assertEquals(postAddressModelResponse.getResults().get(0).getLocations().get(0).getAdminArea5(), "Zaporizhzhia");
        assertEquals(postAddressModelResponse.getResults().get(0).getLocations().get(0).getAdminArea1(), "UA");
        assertEquals(postAddressModelResponse.getResults().get(0).getLocations().get(0).getPostalCode(), "69000-69499");
        assert (postAddressModelResponse.getResults().get(0).getLocations().get(0).getLatLng().getLat().equals(47.841806));
        assert (postAddressModelResponse.getResults().get(0).getLocations().get(0).getLatLng().getLng().equals(35.116526));
        assertEquals(postAddressModelResponse.getResults().get(0).getLocations().get(0).getDisplayLatLng().getLat(),
                postAddressModelResponse.getResults().get(0).getLocations().get(0).getLatLng().getLat());
        assertEquals(postAddressModelResponse.getResults().get(0).getLocations().get(0).getDisplayLatLng().getLng(),
                postAddressModelResponse.getResults().get(0).getLocations().get(0).getLatLng().getLng());
    }

    @Test
    public void testPostEmptyAddress(){

        Options options = new Options().randomize();

        PostAddressModel postAddressModel = new PostAddressModel().randomize(options)
                .setLocation("");

        PostAddressModelResponse postAddressModelResponse = addressService
                .post_addressService(API_KEY, postAddressModel)
                .shouldHave(statusCode(200))
                .responseAs(PostAddressModelResponse.class);

        Assert.assertEquals(postAddressModelResponse.getInfo().getStatuscode(), 400);
        assertEquals(postAddressModelResponse.getInfo().getMessages().get(0),
                "Illegal argument from request: Missing location object in JSON request. Please see the documentation for the Geocoding Service at http://www.mapquestapi.com/geocoding/ for details on correctly formatting requests.");
    }

    @Test
    public void testPostAddressEmptyKey() {

        Options options = new Options().randomize();

        PostAddressModel postAddressModel = new PostAddressModel().randomize(options);

        addressService
                .post_addressService("", postAddressModel)
                .shouldHave(statusCode(403),
                        bodyField( containsString("The AppKey submitted with this request is invalid.")));
    }

    @Test
    public void testGetAddressSuccess(){

        GetAddressModelResponse getAddressModelResponse = addressService
                .get_addressService(API_KEY, address)
                .shouldHave(statusCode(200))
                .responseAs(GetAddressModelResponse.class);

        assertEquals(getAddressModelResponse.getInfo().getStatuscode(), 0);
        assertEquals(getAddressModelResponse.getResults().get(0).getLocations().get(0).getStreet(), "Peremohy Street");
        assertEquals(getAddressModelResponse.getResults().get(0).getLocations().get(0).getAdminArea5(), "Zaporizhzhia");
        assertEquals(getAddressModelResponse.getResults().get(0).getLocations().get(0).getAdminArea1(), "UA");
        assertEquals(getAddressModelResponse.getResults().get(0).getLocations().get(0).getPostalCode(), "69000-69499");
        assert (getAddressModelResponse.getResults().get(0).getLocations().get(0).getLatLng().getLat().equals(47.841806));
        assert (getAddressModelResponse.getResults().get(0).getLocations().get(0).getLatLng().getLng().equals(35.116526));
        assertEquals(getAddressModelResponse.getResults().get(0).getLocations().get(0).getDisplayLatLng().getLat(),
                getAddressModelResponse.getResults().get(0).getLocations().get(0).getLatLng().getLat());
        assertEquals(getAddressModelResponse.getResults().get(0).getLocations().get(0).getDisplayLatLng().getLng(),
                getAddressModelResponse.getResults().get(0).getLocations().get(0).getLatLng().getLng());
    }

    @Test
    public void testGetEmptyAddress(){

        GetAddressModelResponse getAddressModelResponse = addressService
                .get_addressService(API_KEY, "")
                .shouldHave(statusCode(200))
                .responseAs(GetAddressModelResponse.class);

        Assert.assertEquals(getAddressModelResponse.getInfo().getStatuscode(), 400);
        assertEquals(getAddressModelResponse.getInfo().getMessages().get(0),"Illegal argument from request: Insufficient info for location");
    }

    @Test
    public void testGetAddressEmptyKey() {

        addressService
                .get_addressService("", address)
                .shouldHave(statusCode(403),
                        bodyField( containsString("The AppKey submitted with this request is invalid.")));
    }

    @Test
    public void postGetCrossTest(){

        String address = getFakerAddress();

        GetAddressModelResponse getAddressModelResponse = addressService
                .get_addressService(API_KEY, address)
                .shouldHave(statusCode(200))
                .responseAs(GetAddressModelResponse.class);

        Options options = new Options().randomize()
                .setMaxResults(getAddressModelResponse.getResults().get(0).getLocations().size());

        PostAddressModel postAddressModel = new PostAddressModel().randomize(options)
                .setLocation(address);

        PostAddressModelResponse postAddressModelResponse = addressService
                .post_addressService(API_KEY, postAddressModel)
                .shouldHave(statusCode(200))
                .responseAs( PostAddressModelResponse.class);

        assertEquals(getAddressModelResponse.getResults().get(0).getProvidedLocation().getLocation(),
                postAddressModelResponse.getResults().get(0).getProvidedLocation().getStreet());

        for(int i = 0; i<getAddressModelResponse.getResults().get(0).getLocations().size(); i++) {
            assertEquals(getAddressModelResponse.getResults().get(0).getLocations().get(i).getStreet(),
                    postAddressModelResponse.getResults().get(0).getLocations().get(i).getStreet());
            assertEquals(getAddressModelResponse.getResults().get(0).getLocations().get(i).getAdminArea1(),
                    postAddressModelResponse.getResults().get(0).getLocations().get(i).getAdminArea1());
            assertEquals(getAddressModelResponse.getResults().get(0).getLocations().get(i).getAdminArea1Type(),
                    postAddressModelResponse.getResults().get(0).getLocations().get(i).getAdminArea1Type());
            assertEquals(getAddressModelResponse.getResults().get(0).getLocations().get(i).getAdminArea3(),
                    postAddressModelResponse.getResults().get(0).getLocations().get(i).getAdminArea3());
            assertEquals(getAddressModelResponse.getResults().get(0).getLocations().get(i).getAdminArea3Type(),
                    postAddressModelResponse.getResults().get(0).getLocations().get(i).getAdminArea3Type());
            assertEquals(getAddressModelResponse.getResults().get(0).getLocations().get(i).getAdminArea4(),
                    postAddressModelResponse.getResults().get(0).getLocations().get(i).getAdminArea4());
            assertEquals(getAddressModelResponse.getResults().get(0).getLocations().get(i).getAdminArea4Type(),
                    postAddressModelResponse.getResults().get(0).getLocations().get(i).getAdminArea4Type());
            assertEquals(getAddressModelResponse.getResults().get(0).getLocations().get(i).getAdminArea5(),
                    postAddressModelResponse.getResults().get(0).getLocations().get(i).getAdminArea5());
            assertEquals(getAddressModelResponse.getResults().get(0).getLocations().get(i).getAdminArea5Type(),
                    postAddressModelResponse.getResults().get(0).getLocations().get(i).getAdminArea5Type());
            assertEquals(getAddressModelResponse.getResults().get(0).getLocations().get(i).getAdminArea6(),
                    postAddressModelResponse.getResults().get(0).getLocations().get(i).getAdminArea6());
            assertEquals(getAddressModelResponse.getResults().get(0).getLocations().get(i).getAdminArea6Type(),
                    postAddressModelResponse.getResults().get(0).getLocations().get(i).getAdminArea6Type());
            assertEquals(getAddressModelResponse.getResults().get(0).getLocations().get(i).getPostalCode(),
                    postAddressModelResponse.getResults().get(0).getLocations().get(i).getPostalCode());
            assertEquals(getAddressModelResponse.getResults().get(0).getLocations().get(i).getGeocodeQualityCode(),
                    postAddressModelResponse.getResults().get(0).getLocations().get(i).getGeocodeQualityCode());
            assertEquals(getAddressModelResponse.getResults().get(0).getLocations().get(i).getGeocodeQuality(),
                    postAddressModelResponse.getResults().get(0).getLocations().get(i).getGeocodeQuality());
            assertEquals(getAddressModelResponse.getResults().get(0).getLocations().get(i).getDragPoint(),
                    postAddressModelResponse.getResults().get(0).getLocations().get(i).getDragPoint());
            assertEquals(getAddressModelResponse.getResults().get(0).getLocations().get(i).getSideOfStreet(),
                    postAddressModelResponse.getResults().get(0).getLocations().get(i).getSideOfStreet());
            assertEquals(getAddressModelResponse.getResults().get(0).getLocations().get(i).getLinkId(),
                    postAddressModelResponse.getResults().get(0).getLocations().get(i).getLinkId());
            assertEquals(getAddressModelResponse.getResults().get(0).getLocations().get(i).getType(),
                    postAddressModelResponse.getResults().get(0).getLocations().get(i).getType());
            assertEquals(getAddressModelResponse.getResults().get(0).getLocations().get(i).getLatLng().getLat(),
                    postAddressModelResponse.getResults().get(0).getLocations().get(i).getLatLng().getLat());
            assertEquals(getAddressModelResponse.getResults().get(0).getLocations().get(i).getLatLng().getLng(),
                    postAddressModelResponse.getResults().get(0).getLocations().get(i).getLatLng().getLng());
            assertEquals(getAddressModelResponse.getResults().get(0).getLocations().get(i).getDisplayLatLng().getLat(),
                    postAddressModelResponse.getResults().get(0).getLocations().get(i).getDisplayLatLng().getLat());
            assertEquals(getAddressModelResponse.getResults().get(0).getLocations().get(i).getDisplayLatLng().getLng(),
                    postAddressModelResponse.getResults().get(0).getLocations().get(i).getDisplayLatLng().getLng());
        }
    }
}
