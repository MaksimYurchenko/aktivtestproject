package com.aktivcorp.api.geocoder.service.addressService;

import com.aktivcorp.api.geocoder.AssertableResponse;
import com.aktivcorp.api.geocoder.models.addressModels.postAddressModel.PostAddressModel;
import com.aktivcorp.api.geocoder.service.GeocodeSetupApiService;
import io.qameta.allure.Step;
import io.restassured.response.Response;


public class AddressService extends GeocodeSetupApiService {

        @Step("Send post address request")
        public AssertableResponse post_addressService(String apiKey, PostAddressModel postAddressModel) {

            Response register =
                    setupApi()
                            .body(postAddressModel)
                            .when()
                            .post("address?key="+apiKey)
                            .then().extract().response();
            return new AssertableResponse(register);
        }

        @Step("Send get address request")
        public AssertableResponse get_addressService(String apiKey, String address) {

            Response register =
                    setupApi()
                            .when()
                            .get("address?key="+apiKey+"&location="+address)
                            .then().extract().response();
            return new AssertableResponse(register);
        }
}
