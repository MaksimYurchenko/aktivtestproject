package com.aktivcorp.api.geocoder.service;

import io.restassured.RestAssured;
import io.restassured.filter.log.RequestLoggingFilter;
import io.restassured.filter.log.ResponseLoggingFilter;
import io.restassured.http.ContentType;
import io.restassured.specification.RequestSpecification;

import static com.aktivcorp.api.geocoder.properties.UrlLinks.MAIN_URL;

public class GeocodeSetupApiService {

    protected RequestSpecification setupApi(){
        return RestAssured.given()
                .baseUri(MAIN_URL)
                .contentType(ContentType.JSON)
                .filters(new RequestLoggingFilter(),
                        new ResponseLoggingFilter());
    }
}
