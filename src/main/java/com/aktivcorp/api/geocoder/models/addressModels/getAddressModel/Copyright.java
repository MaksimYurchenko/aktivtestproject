package com.aktivcorp.api.geocoder.models.addressModels.getAddressModel;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.ToString;
import lombok.experimental.Accessors;

@Getter
@ToString
@Accessors(chain = true)

public class Copyright{

    @JsonProperty("imageAltText")
    private String imageAltText;

    @JsonProperty("imageUrl")
    private String imageUrl;

    @JsonProperty("text")
    private String text;
}