package com.aktivcorp.api.geocoder.models.addressModels.getAddressModel;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.ToString;
import lombok.experimental.Accessors;

@Getter
@ToString
@Accessors(chain = true)

public class DisplayLatLng{

    @JsonProperty("lng")
    private Double lng;

    @JsonProperty("lat")
    private Double lat;
}