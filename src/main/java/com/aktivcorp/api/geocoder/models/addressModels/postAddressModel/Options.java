package com.aktivcorp.api.geocoder.models.addressModels.postAddressModel;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Accessors;
//for work this annotation need install Lombok plugin
@NoArgsConstructor
@Getter
@Setter
@ToString
@Accessors(chain = true)

public class Options{

    @JsonProperty("thumbMaps")
    private Boolean thumbMaps;

    @JsonProperty("maxResults")
    private Integer maxResults;

    @JsonProperty("ignoreLatLngInput")
    private Boolean ignoreLatLngInput;

    public Options randomize(){
        this.thumbMaps = true;
        this.maxResults = 1;
        this.ignoreLatLngInput = false;

        return this;
    }
}