package com.aktivcorp.api.geocoder.models.addressModels.postAddressModel.response;

import java.util.List;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.ToString;
import lombok.experimental.Accessors;

@Getter
@ToString
@Accessors(chain = true)

public class PostAddressModelResponse{

    @JsonProperty("options")
    private Options options;

    @JsonProperty("results")
    private List<ResultsItem> results;

    @JsonProperty("info")
    private Info info;
}