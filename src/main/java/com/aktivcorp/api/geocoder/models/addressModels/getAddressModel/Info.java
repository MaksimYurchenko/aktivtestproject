package com.aktivcorp.api.geocoder.models.addressModels.getAddressModel;

import java.util.List;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.ToString;
import lombok.experimental.Accessors;

@Getter
@ToString
@Accessors(chain = true)

public class Info{

    @JsonProperty("statuscode")
    private int statuscode;

    @JsonProperty("copyright")
    private Copyright copyright;

    @JsonProperty("messages")
    private List<Object> messages;
}