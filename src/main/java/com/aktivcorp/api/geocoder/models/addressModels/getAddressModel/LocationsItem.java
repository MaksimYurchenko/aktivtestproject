package com.aktivcorp.api.geocoder.models.addressModels.getAddressModel;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.ToString;
import lombok.experimental.Accessors;

@Getter
@ToString
@Accessors(chain = true)

public class LocationsItem{

    @JsonProperty("dragPoint")
    private Boolean dragPoint;

    @JsonProperty("displayLatLng")
    private DisplayLatLng displayLatLng;

    @JsonProperty("adminArea4")
    private String adminArea4;

    @JsonProperty("unknownInput")
    private String unknownInput;

    @JsonProperty("adminArea5")
    private String adminArea5;

    @JsonProperty("adminArea6")
    private String adminArea6;

    @JsonProperty("postalCode")
    private String postalCode;

    @JsonProperty("adminArea1")
    private String adminArea1;

    @JsonProperty("adminArea3")
    private String adminArea3;

    @JsonProperty("sideOfStreet")
    private String sideOfStreet;

    @JsonProperty("type")
    private String type;

    @JsonProperty("adminArea6Type")
    private String adminArea6Type;

    @JsonProperty("geocodeQualityCode")
    private String geocodeQualityCode;

    @JsonProperty("adminArea4Type")
    private String adminArea4Type;

    @JsonProperty("linkId")
    private String linkId;

    @JsonProperty("street")
    private String street;

    @JsonProperty("adminArea5Type")
    private String adminArea5Type;

    @JsonProperty("mapUrl")
    private String mapUrl;

    @JsonProperty("geocodeQuality")
    private String geocodeQuality;

    @JsonProperty("adminArea1Type")
    private String adminArea1Type;

    @JsonProperty("adminArea3Type")
    private String adminArea3Type;

    @JsonProperty("latLng")
    private LatLng latLng;
}