package com.aktivcorp.api.geocoder.models.addressModels.postAddressModel;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Accessors;

import static com.aktivcorp.utilites.dataGenerator.UserDataGeneratorV3.getFakerAddress;
//for work this annotation need install Lombok plugin
@NoArgsConstructor
@Getter
@Setter
@ToString
@Accessors(chain = true)

public class PostAddressModel{

    @JsonProperty("options")
    private Options options;

    @JsonProperty("location")
    private String location;

    public PostAddressModel randomize(Options options){
        this.location = getFakerAddress();
        this.options = options;

        return this;
    }
}