package com.aktivcorp.api.geocoder.models.addressModels.getAddressModel;

import java.util.List;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.ToString;
import lombok.experimental.Accessors;

@Getter
@ToString
@Accessors(chain = true)

public class GetAddressModelResponse{

    @JsonProperty("options")
    private Options options;

    @JsonProperty("results")
    private List<ResultsItem> results;

    @JsonProperty("info")
    private Info info;
}