package com.aktivcorp.api.geocoder.models.addressModels.getAddressModel;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.ToString;
import lombok.experimental.Accessors;

@Getter
@ToString
@Accessors(chain = true)

public class Options{

    @JsonProperty("thumbMaps")
    private Boolean thumbMaps;

    @JsonProperty("maxResults")
    private Integer maxResults;

    @JsonProperty("ignoreLatLngInput")
    private Boolean ignoreLatLngInput;
}