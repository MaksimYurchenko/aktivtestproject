package com.aktivcorp.api.geocoder.models.addressModels.postAddressModel.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.ToString;
import lombok.experimental.Accessors;

@Getter
@ToString
@Accessors(chain = true)

public class ProvidedLocation{

    @JsonProperty("street")
    private String street;
}