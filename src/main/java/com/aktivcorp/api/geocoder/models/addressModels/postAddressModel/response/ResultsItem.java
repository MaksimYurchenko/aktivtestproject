package com.aktivcorp.api.geocoder.models.addressModels.postAddressModel.response;

import java.util.List;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.ToString;
import lombok.experimental.Accessors;

@Getter
@ToString
@Accessors(chain = true)

public class ResultsItem{

    @JsonProperty("locations")
    private List<LocationsItem> locations;

    @JsonProperty("providedLocation")
    private ProvidedLocation providedLocation;
}