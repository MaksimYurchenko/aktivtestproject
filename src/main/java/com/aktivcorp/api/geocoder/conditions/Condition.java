package com.aktivcorp.api.geocoder.conditions;

import io.restassured.response.Response;

public interface Condition {

    void check(Response response);

}

