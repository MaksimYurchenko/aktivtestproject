package com.aktivcorp.api.geocoder.conditions;

import io.restassured.response.Response;
import lombok.AllArgsConstructor;
import org.hamcrest.Matcher;

@AllArgsConstructor
public class BodyFieldConditionText implements Condition {

    private Matcher matcher;

    @Override
    public void check(Response response) {
        response.then().assertThat()
                .body(matcher).toString();
    }

    @Override
    public String toString() {
        return "bodyField " + matcher;
    }
}

