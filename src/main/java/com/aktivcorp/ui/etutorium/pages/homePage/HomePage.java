package com.aktivcorp.ui.etutorium.pages.homePage;

import com.aktivcorp.ui.etutorium.pages.login.LoginPage;
import com.codeborne.selenide.Selenide;
import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.*;

public class HomePage {

    public SelenideElement addWebinar = $("body > div.content-wrapper > main > div.main-content > ui-view > div > div > div > svg > use");

    public static HomePage open(){
        return Selenide.open("https://my.etutorium.com/webinars/active/1", HomePage.class);
    }


}
