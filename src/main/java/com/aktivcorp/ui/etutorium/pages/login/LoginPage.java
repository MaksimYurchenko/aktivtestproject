package com.aktivcorp.ui.etutorium.pages.login;

import com.aktivcorp.ui.etutorium.Navigation;
import com.aktivcorp.ui.etutorium.model.login.UserUiModel;
import com.codeborne.selenide.Condition;
import com.codeborne.selenide.Selenide;
import com.codeborne.selenide.SelenideElement;
import com.codeborne.selenide.WebDriverRunner;
import org.openqa.selenium.JavascriptExecutor;

import static com.codeborne.selenide.Selenide.$;
import static com.aktivcorp.utilites.WaitHelper.sleep;

public class LoginPage {

    private SelenideElement login = $("[id=login_username]");
    private SelenideElement password = $("[id=login_password]");
    private SelenideElement submitButton = $("[type=submit]");

    JavascriptExecutor javascript = (JavascriptExecutor) WebDriverRunner.getWebDriver();

    public static LoginPage open() {
        return Selenide.open("https://passport.etutorium.com/auth/login/etutorium/ru", LoginPage.class);
    }

    public Navigation loginAs(UserUiModel user) {

        sleep(1000);
        login.shouldHave(Condition.enabled);
        password.shouldHave(Condition.enabled);
        submitButton.shouldHave(Condition.enabled);
//        login.setValue(user.getLogin());
        fillEmail(user.getLogin());
        sleep(1000);
        password.setValue(user.getPassword());
        sleep(1000);
//        submitButton.click();
        clickSubmit();
        sleep(15000);
        return new Navigation();
    }

     public void fillEmail(String email) {

         javascript.executeScript("var element = document.querySelector('#login_username'); element.value = '"+email+"';");
    }

    public void clickSubmit() {

        javascript.executeScript("arguments[0].click();",submitButton);
    }
}

