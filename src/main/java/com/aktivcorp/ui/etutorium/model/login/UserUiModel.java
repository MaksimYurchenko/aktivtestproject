package com.aktivcorp.ui.etutorium.model.login;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Accessors;
//for work this annotation need install Lombok plugin
@Getter
@Setter
@ToString
@Accessors(chain = true)
@NoArgsConstructor
public class UserUiModel {

    private String login = "regtstr@gmail.com";
    private String password = "qaz111";
}
