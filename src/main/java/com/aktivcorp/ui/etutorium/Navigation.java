package com.aktivcorp.ui.etutorium;

public class Navigation {

    public <T> T navigatesTo(Class<T> tClass) {
        try {
            return tClass.newInstance();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
