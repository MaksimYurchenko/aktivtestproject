package com.aktivcorp.utilites;

import com.codeborne.selenide.WebDriverRunner;
import lombok.SneakyThrows;

public class WaitHelper {

    @SneakyThrows
    public static void sleep(long millis){
        Thread.sleep(millis);
    }

}
